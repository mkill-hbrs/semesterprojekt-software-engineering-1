
public class IsWellSorted {
	
	static public boolean isWellSorted (String[] sequence) {
		// statische allowance
		String allowance[][] = new String[][] {{"A","C"}, {"C","D"}, {"B","C"}};
		
		// teste ganze sequence
		for (int i = 0; i < sequence.length; i++) {
			
			// teste alle allowance patterns
			for (int k = 0; k < allowance.length; k++) {
				// falls allowance Regel für Position gefunden
				if (sequence[i].equals(allowance[k][1])) {
					
					if (i == 0) {
						// hat Abhängigkeit an Posisition 1
						return false;
					}
					
					// Teste an Position 2+
					for ( int j = i-1; j >= 0; j--) {
						// existieren an vorherigen Positionen Zeichen die nicht der allowance entsprechen?
						
						if (sequence[j].equals(allowance[k][0])) {
							// Abhängigkeit gefunden
							break;
						}
						if (j == 0) {
							// keine Abhängigkeit gefunden vor Position
							return false;
						}
						
					}
					
				}
				
			}
			
			
		}
		
		return true;
	}
	
}
