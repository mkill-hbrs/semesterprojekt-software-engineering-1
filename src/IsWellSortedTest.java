import static org.junit.jupiter.api.Assertions.*;

import java.util.function.BooleanSupplier;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IsWellSortedTest implements IsWellSortedIF{
	private String allowance;
	
	@BeforeEach
	void setUp() throws Exception {
		String allowance[][] = new String[][] {{"A","C"}, {"C","D"}, {"B","C"}};
	}
			
	
	@Test
	void testZeroInput() {
		assertTrue (IsWellSorted.isWellSorted(new String[] {""}));
	}
	
	@Test
	void testOneInput() {
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"B"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"C"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"D"}));
	}
	
	@Test
	void testTwoInput() {
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","B"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"C","B"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"C","D"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"C","A"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","A"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"B","B"}));
	}
	
	@Test
	void testThreeInput() {
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","B","C"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","B","A"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"A","D","C"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"A","C","D"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"B","A","C"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"D","A","B"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"B","D","A"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","A","A"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"C","C","D"}));
	}
	
	@Test
	void testIsWellSorted() {
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","B","C","D"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"A","B","D","C"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","A","A","A"}));
		assertFalse(IsWellSorted.isWellSorted(new String[] {"A","C","A","D"}));
	}
	
	@Test
	void testEdgyInput() {
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","B","C","D","C","C","C","D"}));
		assertTrue (IsWellSorted.isWellSorted(new String[] {"A","B","C","D","C","C","C","D","A","B","C","D","C","C","C","D"}));
	}
}